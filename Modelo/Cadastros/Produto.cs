﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Modelo.Tabelas;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Modelo.Cadastros
{
    public class Produto
    {
        [DisplayName("ID")] public long? ProdutoId { get; set; }

        [StringLength(100,ErrorMessage ="O nome precisa ter no mínimo 2 caracteres",MinimumLength =2)]
        [DisplayName("Nome")]
        [Required(ErrorMessage ="Informe o nome do produto")]
        public string Nome { get; set; }

        [DisplayName("Data de Cadastro")]
        [Required(ErrorMessage ="Informe a data do cadastro")]
        public DateTime? DataCadastro { get; set; }

        [DisplayName("Categoria")]
        public long? CategoriaId { get; set; }

        [DisplayName("Fabricante")]
        public long? FabricanteId { get; set; }

        public string LogotipoMineType { get; set; }
        public byte[] Logotipo { get; set; }

        public Categoria Categoria { get; set; }
        public Fabricante Fabricante { get; set; }
    }
}